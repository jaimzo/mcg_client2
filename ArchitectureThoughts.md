# General Architecture
Here are some brief thoughts on the architecture of the solution.

The advantage of the approach used is its flexibility: document-oriented systems are quicker to prototype with when prototyping different solutions or clients – i.e. we don't need to worry about schemas or DB migrations, and the data 'shape' is similar between client and server. It is also easy to deploy on AWS and the Lambda code is nicely focused on isolated responsibilities.

The architecture is motivated by the idea that it is easier to scale a system without a database but I'm not sure that an RDBMS would be the bottleneck in this scenario, or at least that other techniques could not be used to scale an RDBMS solution (for example, sharding database instances according to, say, a hash of some organisation ID). Moreover, to get useful aggregate stats from the quiz responses we would have to make a semi-bespoke system that downloaded the data from S3 and interrogated the collective JSON. For any sophisticated query we may be well on our way to re-inventing a lot of what a traditional database can do for us, perhaps including concurrency control and serialisation. New products are seldom in need of such bespoke scalability solutions.

In "real life" I'd be tempted to go with a more traditional DB solution such as Postgres and use its embedded [JSON support](https://www.postgresql.org/docs/9.4/datatype-json.html) to enable document-db style storage during development whilst still giving us a path to a more structured, queryable store as the product evolves.

The only other aspect that strikes me is the use of numeric IDs in the question and answer JSON structures. Arguably answers only need IDs that are unique within the context of their specific question, so numeric IDs make sense: although should they monotonically increase across questions? The ID of the first answer in the second question is '2' rather than '0'. Answers are already modelled as an array within the question structure so they have an implicit ID which is their index within that array.

Numeric ID for questions and quizzes themselves may be a little trickier. IDs for quizzes need to be globally unique and potentially robust in the face of concurrent creation of quizzes. IDs for questions need only be unique within the context of the quiz the question appears in, but we may want to share questions between quizzes in future. I'd be tempted to use UUIDs for both quiz and question IDs.

## Server
Neither my Go or AWS knowledge is good enough to give an in-depth analysis here but the code in general seems nicely compartmentalised and focused on specific jobs within the system, although the implicit dependencies between them are non-obvious (e.g. what is the JSON schema of the various objects, which lambdas are listening to which FIFOs etc.)

The scalability properties are unclear to me but this may be my lack of experience with Lambda. For example, right now we have a chain of Lambdas triggered either by a FIFO message or an S3 event: what is the scalability limit to this? Can we scale up via parallelisation and if so how does S3 cope? Do we need to worry about concurrency control and serialisation in the face of many writes? In reality I suspect that this will never be a write-heavy system (compared to reads) and we do not require strict consistency guarantees – i.e. we can derive meaningful stats without completely reconciling all responses.

There does seem to be a bug in which the first entry in the `answerData` array written by the `listenCapture` Lambda always has an empty `answerId` and a `null` `responseFile` but I haven't nailed down why that is happening yet.

## Client
The client code was a little over-complex, presumably because of the dev team's unfamiliarity with the technology and create-react-app's tendency to generate a lot more code than is needed. I tried to simplify this project down having had the benefit making many of these mistakes myself. Mainly, I removed the context code and simplified the reducers and some of the visual components, we can also drive the transition between one question and the next implicitly via a state / reducer update rather than having to pass around the `nextPage` callback.

Possibly a page-oriented framework such as [`Next.js`](https://nextjs.org/) would be better suited to this project. This would allow us to use browser navigation (e.g. the back button), allow links to specific quizzes and questions, and allow server-side rendering of the UI.