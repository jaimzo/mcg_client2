import axios from 'axios'
import data from "./QuizData";


// Used to denote reducer actions
const StoreAction = "store-action";
const AnswerAction = "answer-action";


// NOTE: This should be a network call to load the quiz 
// with ID "quizId".
// Just use hard-coded questions for now.
const loadQuiz = (/*quizId*/) => {

    // Massage the question data into a Map. Preferably this should
    // be done server-side
    const {
        id = -1,
        title = "",
        questions = [] } = data;

    if (id === -1)
        console.warn("No ID in quiz?");

    // Put the questions into a map for fast lookup
    // This is possibly overkill if questions are always
    // stored in linear order in an array. Linear lookup or
    // binary search will be more than fast enough.
    let questionMap = new Map();
    for (let q of questions) {
        questionMap.set(q.id, q);
    }

    
    return {
        id, title,
        
        questions: questionMap,
        
        // NOTE: Assume '0' is always the id of 
        // the first question
        currentQuestion: questionMap.get(0),
        
        answers: new Map(),
        
        complete: false
    };
};


// Make a network call to save the quiz answers
const saveAnswers = async (quiz) => {
    const apiEndpoint = `${window.awsAPI.apiEndpoint}?quizId=${quiz.id}`;
    
    let results = [];
    quiz.answers.forEach(
        (value, key) => {
            results.push({
                id: `${key}`,
                answers: Array.isArray(value) ? value : [`${value}`]
            })
        }
    );
    
    const responseObject = {
        quizId: `${quiz.id}`,
        questionAnswers: results 
    };
    
    await axios.post(apiEndpoint, responseObject);
};


// Answer the current question with answer 'answerId'
const answerQuestion = (quiz, answerId) => {
    const { currentQuestion } = quiz;
    quiz.answers.set(currentQuestion.id, answerId);
    
    // NOTE: O(n) lookup is fine with the numbers we're dealing with...
    const answer = currentQuestion.answers.find((a) => a.id === answerId);
    const nextQuestionId =  currentQuestion.nextQuestionId || answer.nextQuestionId;
    
    // NOTE: The original code had a "quizData" property mapping to this object.
    // Unclear whether that was deliberate - omitting for now
    return { 
        ...quiz, 
        currentQuestion : quiz.questions.get(nextQuestionId) 
    };
};


// Reducer function that can modify the current quiz
const reduceQuiz = (quiz, action) => {
    let result = quiz;
    switch (action.type) {
        case StoreAction:
            saveAnswers(quiz).then(() => result = { ...quiz, complete: action.update.completed });
            break;
            
        case AnswerAction:
            result = answerQuestion(quiz, action.update.answer);
            break;
            
        default:
            // nothing
            break
    }

    return result
};


export {
    // Note: Can we fold create quiz into reduce quiz?
    // CreateQuiz,
    loadQuiz,
    reduceQuiz,
    
    StoreAction,
    AnswerAction
}