import React, { useReducer } from "react";
import {loadQuiz, reduceQuiz } from "./API/Quiz";

import PageHeader from './components/PageHeader'
import QuizHeader from './components/QuizHeader'
import Question from './components/Question'


const QuizApp = () => {
    // NOTE: This should probably come from the URL via some Router
    // object
    const quizId = 0;
    const [quiz, dispatch] = useReducer(reduceQuiz, quizId, loadQuiz);
    
    
    const { currentQuestion, title } = quiz;
    
    return (
        <main>
            <PageHeader />
            <QuizHeader>{title}</QuizHeader>
            <Question question={currentQuestion} dispatch={dispatch} />
        </main>
    )
};

export default QuizApp;