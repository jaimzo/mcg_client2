import React from "react";
import { AnswerAction } from '../API/Quiz';

import styles from './ButtonResponse.module.scss';

const ButtonResponse = ({answers, dispatch}) => {
  const handleClick = (answerId) => dispatch({ type: AnswerAction, update: { answer : answerId }});
  
  return (
    <div className={styles.buttonRow}>
      {answers.map((answer) =>
        <button key={answer.id}
                onClick={() => handleClick(answer.id)}>
          {answer.text}
        </button>
      )}
    </div>
  )
};

export default ButtonResponse

