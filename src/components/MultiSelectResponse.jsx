import React, { useState } from "react";
import { AnswerAction } from "../API/Quiz";

import styles from './MultiSelectResponse.module.scss';

const MultiSelectResponse = ({answers, dispatch}) => {
  // Seems clumsy to need both of these
  const [selectedSet, setSelectedSet] = useState(new Set());
  const [selectedCount, setSelectedCount] = useState(0);
  
  const doSubmit = () => dispatch({
    type: AnswerAction,
    update: { answer: [...selectedSet]}
  });
  
  return (
    <section className={styles.multiSelectForm}>
      {answers.map((answer) => {
        const stringId = `${answer.id}`;
        
        return (
          <section key={`_${stringId}`}
               className={styles.multiSelectOptions}>
            <input
              id={stringId}
              type="checkbox"
              onChange={
                () => {
                  selectedSet.has(stringId) ? selectedSet.delete(stringId) : selectedSet.add(stringId);
                  setSelectedSet(selectedSet);
                  setSelectedCount(selectedSet.size);
                }
              }
              defaultChecked={selectedSet.has(stringId)}
            />
            <label htmlFor={stringId}>{answer.text}</label>
          </section>
        )
      })}
      
      {selectedCount === 0 && <div className={styles.prompt }>Please select some answers...</div>}
      {selectedCount > 0 && <div className={styles.continue}><button onClick={doSubmit}>Continue...</button></div>}
    </section>
  )
};

export default MultiSelectResponse;