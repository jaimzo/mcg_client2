import React from "react";
import styles from "./PageHeader.module.scss"

const PageHeader = () => {
  return (<h1 className={styles.pageHeader}>the circle</h1>)
};

export default PageHeader