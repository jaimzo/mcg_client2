import React from "react";
import ButtonResponse from './ButtonResponse'
import MultiSelectResponse from './MultiSelectResponse'
import TextResponse from './TextResponse'

import styles from './Question.module.scss'

const UnknownTypeResponse = () => <div className="error">Unknown question type</div>

const responderComponentFor = (question) => {
  let result = null;
  switch (question.type) {
    case "button":
      result = ButtonResponse;
      break;
    case "multiSelect":
      result = MultiSelectResponse;
      break;
    case "text":
      result = TextResponse;
      break;
    default:
      console.warn('Unknown question type: ', question.type);
      result = UnknownTypeResponse;
      break;
  }

  return result
};

const Question = ({question, dispatch}) => {
  const ResponderComponent = responderComponentFor(question);
  
  return (
    <section className={styles.question}>
      <section className={styles.questionText}>
        {question.title}
      </section>

      {ResponderComponent &&
        <ResponderComponent answers={question.answers} dispatch={dispatch}/>
      }
    </section>
  )
};

export default Question