import React from "react";
import styles from './QuizHeader.module.scss'

// Seems overkill, but allows us to continue using modular CSS
const QuizHeader = ({children}) => <h1 className={styles.quizHeader}>{children}</h1>;

export default QuizHeader;
