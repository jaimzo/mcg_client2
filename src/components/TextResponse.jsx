import React, { useEffect } from "react";
import { StoreAction } from "../API/Quiz";

// We take a "text" response to be the equivalent of terminating the quiz
// Should probably be an explicit question "type" (or abstract quiz sections
// into questions and meta elements should as the terminator)
const TextResponse = ({dispatch}) => {
  useEffect(() => {
    dispatch({ 
      type: StoreAction, 
      update: {
        completed: true
      }})
  }, [dispatch]);
  
  return (<div />)
};

export default TextResponse