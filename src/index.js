import React from 'react';
import ReactDOM from 'react-dom';
import './styling/index.scss';
import QuizApp from "./QuizApp";
//import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<QuizApp />, document.getElementById('root'));
